package com.shahsk.arrays._561;

import java.util.*;

public class ArrayPartitionI {

    /*
     *  Initialize numBucket array with all zeros, stores count of occurence of each number.
     *
     *  First for-loop virtually places each number in it's respective position in ascending order in array,
     *  with it's occurrence count being stored as the value, which gets incremented every time the number,
     *  is encountered again.
     *  Ex -> num = 100, numBucket[10100]++ => numBucket[10100] = 1;
     *  Ex -> num = -10000, numBucket[0]++ => numBucket[9900] = 1;
     *
     *  Now all that's left is to read the whole array in order and get alternate values from array &
     *  it's count.
     *  flag controls which number to add to sum i.e. takes minimum number alternately. (mininum of 2)
     **/

    public int arrayPairSum_1(int[] nums) {


        int[] numBucket = new int[20001];
        for(int num: nums) {
            numBucket[num+10000]++;
        }


        boolean flag = true;
        int sum = 0;
        for(int i=0;i<20001;i++) {

            while(numBucket[i]>0) {
                // Retrieve the original value of number and add to sum
                if(flag) {
                    sum+= i>10000?i-10000:-(10000-i);
                }

                // Skip the next number
                flag=!flag;

                // Reduce the occurrence count of number
                numBucket[i]--;
            }
        }

        return sum;
    }


    public int arrayPairSum_2(int[] nums) {
        Arrays.sort(nums);
        int sum = 0;
        for(int i=0;i<nums.length;i+=2)
         sum+=nums[i];

        return sum;
    }
}